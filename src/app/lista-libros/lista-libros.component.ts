import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { LibrosApiClient } from '../models/libro-api-client.model';
import { Libro } from '../models/libro.model';

@Component({
  selector: 'app-lista-libros',
  templateUrl: './lista-libros.component.html',
  styleUrls: ['./lista-libros.component.css']
})
export class ListaLibrosComponent implements OnInit {
	@Output() onItemAdded: EventEmitter<Libro>;
  constructor(public librosApiClient: LibrosApiClient ) {
  	this.onItemAdded = new EventEmitter();
  }

  ngOnInit(): void {
  }

  agregado(l: Libro){
    this.librosApiClient.add(l);
    this.onItemAdded.emit();
  }

   contarvoto(l:Libro,tup:boolean){
     //console.log(`tup =${tup}`);
     l.addThumbsUp(tup);
   }
  
}
