import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { Libro } from '../models/libro.model';

@Component({
  selector: 'app-form-libro',
  templateUrl: './form-libro.component.html',
  styleUrls: ['./form-libro.component.css']
})
export class FormLibroComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<Libro>;
  fg: FormGroup;
  lTitulo = 5;
  constructor(private fb: FormBuilder) {
    this.onItemAdded = new EventEmitter();
  	this.fg = this.fb.group({
  		titulo: ['',Validators.compose([
        Validators.required,
        this.valLongTitulo(this.lTitulo)
      ])],
      autor: [''],
      editorial: [''],
      paginas: [''],
      estrellas: [''],
      tapaUrl: ['']
  	}); 
   }

  ngOnInit(): void {
  }

  guardar(titulo:string,autor:string,editorial:string,paginas:string,estrellas:string,tapaUrl:string): boolean{
    const l = new Libro(titulo,autor,editorial,Number(paginas),Number(estrellas),tapaUrl);
  	this.onItemAdded.emit(l);
  	return false;
  }

  valLongTitulo(longMin: number):ValidatorFn{
    return (control: FormControl): {[s: string]:boolean} | null =>{
      const l = control.value.toString().trim().length;
      if (l<longMin){
        return {minLongTitulo:true}
      }
      return null;
    }
  }
}
