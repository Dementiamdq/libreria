import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

//import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LibroComponent } from './libro/libro.component';
import { ListaLibrosComponent } from './lista-libros/lista-libros.component';
import { FormLibroComponent } from './form-libro/form-libro.component';
import { LibroDetalleComponent } from './libro-detalle/libro-detalle.component';
import { LibrosApiClient } from './models/libro-api-client.model';

const routes: Routes = [
  {path:'', redirectTo: 'home', pathMatch: 'full'},
  {path:'home', component: ListaLibrosComponent},
  {path:'libro', component: LibroDetalleComponent}
]
@NgModule({
  declarations: [
    AppComponent,
    LibroComponent,
    ListaLibrosComponent,
    FormLibroComponent,
    LibroDetalleComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
   // AppRoutingModule
  ],
  providers: [LibrosApiClient],
  bootstrap: [AppComponent]
})
export class AppModule { }
