export class Libro{
	public megusta: number;
	public nomegusta: number;
	public encuadernacion: string[];
	
	constructor(public titulo:string,
				public autor:string,
				public editorial:string,
				public paginas:number,
				public calificacion: number,
				public tapaURL:string){
		this.megusta = 0;
		this.nomegusta = 0;
		this.encuadernacion = ['tapa dura','tapa blanda','pocket'];
	}

	addThumbsUp(isUp: boolean){
		console.log(isUp);
		if (isUp) {
			this.megusta++;
			console.log("entro al if");
		}
		else {
			this.nomegusta++;
			console.log("entro al else");
		}
	}

	tieneLikes():boolean{
		return (this.megusta > 0 || this.nomegusta>0);
	}
	
}