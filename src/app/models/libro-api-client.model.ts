import { Libro } from './libro.model';

export class LibrosApiClient{
    libros: Libro[];

    constructor(){
        this.libros = [];
    }

    add(l:Libro){
        this.libros.push(l);
    }

    getAll(){
        return this.libros;
    }
}