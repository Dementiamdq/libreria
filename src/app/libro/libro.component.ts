import { Component, OnInit, Input, HostBinding, Output, EventEmitter } from '@angular/core';
import { Libro } from '../models/libro.model';

@Component({
  selector: 'app-libro',
  templateUrl: './libro.component.html',
  styleUrls: ['./libro.component.css']
})
export class LibroComponent implements OnInit {
  @Input() libro: Libro;
  @Input() indice: number;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() hanvotado: EventEmitter<{event:Libro,thumbsup:boolean}>; 
  //@Output() hanvotado: EventEmitter<Libro>; 
  
  constructor() {
    this.hanvotado = new EventEmitter();
  }

  votar(thumbsup:boolean){
    console.log(thumbsup)
    this.hanvotado.emit({event: this.libro,thumbsup:thumbsup})
//      this.hanvotado.emit(this.libro)
    return false;
  }

  ngOnInit(): void {
  }

}
